import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController _userController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();
  String _userErrorText = "";
  String _emailErrorText = "";
  String _passwordErrorText = "";
  String _confirmPasswordErrorText = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          // Imagen de fondo
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/img/190826fe4729d502150c627581041410.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          // Contenido con transparencia
          Center(
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 32.0).copyWith(
                bottom: MediaQuery.of(context).viewInsets.bottom + 32.0,
              ),
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16.0),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.8),
                  borderRadius: BorderRadius.circular(16.0),
                ),
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'Crear Cuenta',
                        style: TextStyle(
                          fontSize: 32,
                          fontWeight: FontWeight.bold,
                          color: Colors.deepPurple,
                        ),
                      ),
                      SizedBox(height: 20),
                      TextField(
                        controller: _userController,
                        onChanged: (_) {
                          setState(() {
                            _userErrorText = "";
                          });
                        },
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.person),
                          hintText: 'Usuario',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          errorText: _userErrorText.isNotEmpty ? _userErrorText : null,
                        ),
                      ),
                      SizedBox(height: 10),
                      TextField(
                        controller: _emailController,
                        onChanged: (_) {
                          setState(() {
                            _emailErrorText = "";
                          });
                        },
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.email),
                          hintText: 'Correo electrónico',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          errorText: _emailErrorText.isNotEmpty ? _emailErrorText : null,
                        ),
                      ),
                      SizedBox(height: 10),
                      TextField(
                        controller: _passwordController,
                        onChanged: (_) {
                          setState(() {
                            _passwordErrorText = "";
                          });
                        },
                        obscureText: true,
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.lock),
                          hintText: 'Contraseña',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          errorText: _passwordErrorText.isNotEmpty ? _passwordErrorText : null,
                        ),
                      ),
                      SizedBox(height: 10),
                      TextField(
                        controller: _confirmPasswordController,
                        onChanged: (_) {
                          setState(() {
                            _confirmPasswordErrorText = "";
                          });
                        },
                        obscureText: true,
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.lock),
                          hintText: 'Confirmar contraseña',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          errorText: _confirmPasswordErrorText.isNotEmpty ? _confirmPasswordErrorText : null,
                        ),
                      ),
                      SizedBox(height: 20),
                      ElevatedButton(
                        onPressed: () {
                          String username = _userController.text.trim();
                          String email = _emailController.text.trim();
                          String password = _passwordController.text.trim();
                          String confirmPassword = _confirmPasswordController.text.trim();
                          if (username.isEmpty) {
                            setState(() {
                              _userErrorText = "Por favor ingrese su usuario.";
                            });
                          }
                          if (email.isEmpty) {
                            setState(() {
                              _emailErrorText = "Por favor ingrese su correo electrónico.";
                            });
                          }
                          if (password.isEmpty) {
                            setState(() {
                              _passwordErrorText = "Por favor ingrese su contraseña.";
                            });
                          }
                          if (confirmPassword.isEmpty) {
                            setState(() {
                              _confirmPasswordErrorText = "Por favor confirme su contraseña.";
                            });
                          }
                          if (password != confirmPassword) {
                            setState(() {
                              _confirmPasswordErrorText = "Las contraseñas no coinciden.";
                            });
                          }
                          if (username.isNotEmpty && email.isNotEmpty && password.isNotEmpty && password == confirmPassword) {
                            // Aquí puedes realizar el registro con los datos ingresados
                            Navigator.pop(context);
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.deepPurple,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                        ),
                        child: Text(
                          'Registrar',
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context); // Retrocede a la página anterior
                        },
                        child: Text(
                          'Sign in', // Texto para el botón
                          style: TextStyle(
                            color: Colors.deepPurple, // Color del texto
                            fontSize: 16, // Tamaño del texto
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
