import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  // Simulación de recetas favoritas
  final List<String> favoriteRecipes = [
    'Receta 1',
    'Receta 2',
    'Receta 3',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Perfil',
          style: TextStyle(color: Colors.white), // Cambia el color del título a blanco
        ),
        iconTheme: IconThemeData(color: Colors.white), // Cambia el color de los íconos a blanco
        backgroundColor: Colors.transparent, // Hace que el fondo del AppBar sea transparente
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.purple, Colors.deepPurple], // Colores del gradiente
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.restaurant_menu),
            color: Colors.white, // Cambia el color del ícono de cloche a blanco
            onPressed: () {
              // Acción al presionar el icono de cloche
            },
          ),
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/img/190826fe4729d502150c627581041410.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.8),
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Sección de información del usuario
                  Center(
                    child: Column(
                      children: [
                        CircleAvatar(
                          radius: 50.0,
                          backgroundImage: AssetImage('assets/img/cara.png'),
                        ),
                        SizedBox(height: 16.0),
                        Text(
                          'EL MEHDI',
                          style: TextStyle(
                            fontSize: 24.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black, // Texto negro para contrastar con el fondo
                          ),
                        ),
                        SizedBox(height: 8.0),
                        Text(
                          'midood@outlook.com',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.grey[700],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 32.0),

                  // Sección de recetas favoritas
                  ListTile(
                    leading: Icon(
                      Icons.favorite,
                      color: Colors.red,
                    ),
                    title: Text(
                      'RECETAS QUE ME GUSTAN',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black, // Texto negro para contrastar con el fondo
                      ),
                    ),
                  ),
                  SizedBox(height: 16.0),
                  Container(
                    height: 100.0, // Ajusta la altura según sea necesario
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: favoriteRecipes.length,
                      itemBuilder: (context, index) {
                        return Container(
                          width: 150.0,
                          margin: EdgeInsets.symmetric(horizontal: 8.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16.0),
                            color: Colors.deepPurpleAccent,
                          ),
                          child: Center(
                            child: Text(
                              favoriteRecipes[index],
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),

                  // Opciones de configuración
                  SizedBox(height: 32.0),
                  ListTile(
                    leading: Icon(Icons.settings, color: Colors.black),
                    title: Text(
                      'Configuración',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.black, // Texto negro para contrastar con el fondo
                      ),
                    ),
                    onTap: () {
                      // Acción al presionar
                    },
                  ),
                  SizedBox(height: 16.0),
                  ListTile(
                    leading: Icon(Icons.logout, color: Colors.black),
                    title: Text(
                      'Cerrar sesión',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.black,
                      ),
                    ),
                    onTap: () {
                      // Acción al presionar
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
