import 'package:flutter/material.dart';
import 'package:emhafdi_tf_ipc/First_page.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'Proyecto final',
        debugShowCheckedModeBanner: false,
        home: FirstPage(),
      );
}
