class Recipe {
  final String name;
  final String image;
  final List<String> ingredients;
  final String howToCook;

  Recipe({
    required this.name,
    required this.image,
    required this.ingredients,
    required this.howToCook,
  });

  static Recipe fromJson(Map<String, dynamic> json) {
    return Recipe(
      name: json['name'],
      image: json['image'],
      ingredients: List<String>.from(json['ingredients']),
      howToCook: json['howToCook'],
    );
  }
}
