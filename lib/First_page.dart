import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart'; // Importante para cargar archivos JSON
import 'package:emhafdi_tf_ipc/Register_page.dart';
import 'package:emhafdi_tf_ipc/Second_page.dart';
import 'package:emhafdi_tf_ipc/recipe_model.dart';

class FirstPage extends StatefulWidget {
  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  TextEditingController _userController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  String _userErrorText = "";
  String _passwordErrorText = "";
  List<Recipe> recipes = [];

  @override
  void initState() {
    super.initState();
    loadRecipes();
  }

  Future<void> loadRecipes() async {
    final String response = await rootBundle.loadString('assets/img/recetas.json');
    final data = await json.decode(response);
    List<dynamic> jsonRecipes = data['products'];

    List<Recipe> loadedRecipes = jsonRecipes.map((json) => Recipe.fromJson(json)).toList();

    setState(() {
      recipes = loadedRecipes;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/img/190826fe4729d502150c627581041410.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Center(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 32.0),
              margin: EdgeInsets.symmetric(horizontal: 16.0),
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.8),
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Iniciar Sesión',
                    style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.bold,
                      color: const Color.fromARGB(255, 115, 94, 151),
                    ),
                  ),
                  SizedBox(height: 20),
                  TextField(
                    controller: _userController,
                    onChanged: (_) {
                      setState(() {
                        _userErrorText = "";
                      });
                    },
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.person),
                      hintText: 'Usuario',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      errorText: _userErrorText.isNotEmpty ? _userErrorText : null,
                    ),
                  ),
                  SizedBox(height: 10),
                  TextField(
                    controller: _passwordController,
                    onChanged: (_) {
                      setState(() {
                        _passwordErrorText = "";
                      });
                    },
                    obscureText: true,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock),
                      hintText: 'Contraseña',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      errorText: _passwordErrorText.isNotEmpty ? _passwordErrorText : null,
                    ),
                  ),
                  SizedBox(height: 20),
                 ElevatedButton(
  onPressed: () {
    String username = _userController.text.trim();
    String password = _passwordController.text.trim();
    if (username.isEmpty) {
      setState(() {
        _userErrorText = "Por favor ingrese su usuario.";
      });
    }
    if (password.isEmpty) {
      setState(() {
        _passwordErrorText = "Por favor ingrese su contraseña.";
      });
    }
    if (username.isNotEmpty && password.isNotEmpty) {
      // Aquí agregarías tu lógica para verificar el inicio de sesión
      // y mostrar el cuadro de diálogo correspondiente
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Bienvenido"),
            content: Text("¡Inicio de sesión exitoso!"),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SecondPage(recipes: recipes, userName: username)),
                  );
                },
                child: Text("OK"),
              ),
            ],
          );
        },
      );
    }
  },
  style: ElevatedButton.styleFrom(
    backgroundColor: Colors.deepPurple,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(16.0),
    ),
    padding: EdgeInsets.symmetric(vertical: 16.0),
  ),
  child: Text(
                      '       Login       ',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => RegisterPage()),
                      );
                    },
                    child: Text(
                      '¿No tienes cuenta? Regístrate',
                      style: TextStyle(
                        color: Colors.deepPurple,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
